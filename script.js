"use strict";

const modal = document.querySelector('.modal');
const overlay = document.querySelector('.overlay');
const btnCloseModal = document.querySelector('.close-modal');
const btnsOpenModal = document.querySelectorAll('.show-modal');
const closeModal = function() {
    modal.classList.add('hidden');
    overlay.classList.add('hidden');
}
const displayModal = function() {
    modal.classList.remove('hidden');
    overlay.classList.remove('hidden');
}

for (let index = 0; index < btnsOpenModal.length; index++) {
    btnsOpenModal[index].addEventListener('click', displayModal);
}
btnCloseModal.addEventListener('click', closeModal);
overlay.addEventListener('click', closeModal);

document.addEventListener('keydown', function(e) {
    if (e.key === 'Escape' && !modal.classList.contains('hidden')) {
        closeModal();
    }
})